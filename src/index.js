import React from "./lib/react";
import ReactDOM from "./lib/react-dom";
// import React from "react";
// import ReactDOM from "react-dom";
let ThemeContext = React.createContext();

const { Provider, Consumer } = ThemeContext;
let style = { margin: "5px", padding: "5px" };
function Title(props) {
  debugger;

  return (
    <Consumer>
      {(contextValue) => (
        <div style={{ ...style, border: `5px solid ${contextValue.color}` }}>
          Title
        </div>
      )}
    </Consumer>
  );
}
class Header extends React.Component {
  static contextType = ThemeContext;
  render() {
    debugger;

    return (
      <div style={{ ...style, border: `5px solid ${this.context.color}` }}>
        Header
        <Title />
      </div>
    );
  }
}
function Content() {
  debugger;

  return (
    <Consumer>
      {(contextValue) => (
        <div style={{ ...style, border: `5px solid ${contextValue.color}` }}>
          Content
          <button
            style={{ color: "red" }}
            onClick={() => contextValue.changeColor("red")}
          >
            变红
          </button>
          <button
            style={{ color: "green" }}
            onClick={() => contextValue.changeColor("green")}
          >
            变绿
          </button>
        </div>
      )}
    </Consumer>
  );
}
class Main extends React.Component {
  static contextType = ThemeContext;
  render() {
    debugger;

    return (
      <div style={{ ...style, border: `5px solid ${this.context.color}` }}>
        Main
        <Content />
      </div>
    );
  }
}

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = { color: "black" };
  }
  changeColor = (color) => {
    this.setState({ color });
  };
  render() {
    let contextValue = {
      color: this.state.color,
      changeColor: this.changeColor,
    };
    return (
      <Provider value={contextValue}>
        <div
          style={{
            ...style,
            width: "250px",
            border: `5px solid ${this.state.color}`,
          }}
        >
          Page
          <Header />
          <Main />
        </div>
      </Provider>
    );
  }
}

function App() {
  const [number, setNumber] = React.useState(0);
  const [number2, setNumber2] = React.useState(0);

  return (
    <div>
      <p>{number}</p>
      <p>{number2}</p>
      <button
        onClick={() => {
          setNumber(number + 1);
          setNumber2(number2 + 1);
        }}
      >
        +
      </button>
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
