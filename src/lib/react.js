import {
  REACT_ELEMENT,
  REACT_FORWARD_REF_TYPE,
  REACT_CONTEXT,
  REACT_PROVIDER,
} from "./constants";
import { mapToVDom } from "./utils";
import { Component } from "./reactComponent";
import { useState } from "./react-dom";

function createElement(type, config, children) {
  let ref, key;
  if (config) {
    ref = config.ref;
    key = config.key;
    delete config.ref;
    delete config.key;
    delete config.__self__;
    delete config.__source__;
  }

  let props = { ...config };
  if (arguments.length > 3) {
    props.children = Array.prototype.slice.call(arguments, 2).map(mapToVDom);
  } else {
    props.children = mapToVDom(children);
  }
  return {
    $$type: REACT_ELEMENT,
    ref,
    key,
    props,
    type,
  };
}

function createRef() {
  return { current: null };
}

function forwardRef(render) {
  return {
    $$typeof: REACT_FORWARD_REF_TYPE,
    render,
  };
}

function createContext() {
  let context = {
    $$typeof: REACT_CONTEXT,
  };
  context.Provider = {
    $$typeof: REACT_PROVIDER,
    _context: context,
  };
  context.Consumer = {
    $$typeof: REACT_CONTEXT,
    _context: context,
  };
  return context;
}

const Children = {
  map(children, mapFn) {
    return children.flat().map(mapFn);
  },
};

const React = {
  createElement,
  Component,
  createRef,
  forwardRef,
  createContext,
  Children,
  useState,
};

export default React;
