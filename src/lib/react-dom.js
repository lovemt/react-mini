import {
  MOVE,
  PLACEMENT,
  REACT_CONTEXT,
  REACT_FORWARD_REF_TYPE,
  REACT_PROVIDER,
  REACT_TEXT,
} from "./constants";
import { addEvent } from "./events";

//保存状态值的数组
let hookState = [];
let hookIndex = 0;
let scheduleUpdate;

/**
 *
 * @param vdom 虚拟dom
 * @param container 容器
 */
function render(vdom, container) {
  mount(vdom, container);
  scheduleUpdate = () => {
    hookIndex = 0;
    compareTwoVdom(container, vdom, vdom);
  };
}

export function useState(initialState) {
  hookState[hookIndex] = hookState[hookIndex] || initialState;
  const currentIndex = hookIndex;
  function setState(newState) {
    hookState[currentIndex] = newState;

    scheduleUpdate();
  }
  return [hookState[hookIndex++], setState];
}

export function useMemo(factory, deps) {
  if (hookState[hookIndex]) {
    let [oldMemo, oldDeps] = hookState[hookIndex];
    let same = oldDeps.evey((dep, index) => dep === deps[index]);
    if (same) {
      hookIndex++;
      return oldMemo;
    } else {
      let newMemo = factory();
      hookState[hookIndex++] = [newMemo, deps];
      return newMemo;
    }
  } else {
    let newMemo = factory();
    hookState[hookIndex++] = [newMemo, deps];
    return newMemo;
  }
}

export function useCallback(callback, deps) {
  if (hookState[hookIndex]) {
    let [oldCallback, oldDeps] = hookState[hookIndex];
    let same = oldDeps.evey((dep, index) => dep === deps[index]);
    if (same) {
      hookIndex++;
      return oldCallback;
    } else {
      hookState[hookIndex++] = [callback, deps];
      return callback;
    }
  } else {
    hookState[hookIndex++] = [callback, deps];
    return callback;
  }
}

/**
 * 把虚拟dom 挂载到真实容器上面
 * @param vdom 虚拟dom
 * @param container 容器
 */
function mount(vdom, container) {
  let newDOM = createDOM(vdom);
  //挂在dom之后，触发子dom挂载完成的事件
  container.appendChild(newDOM);
  if (newDOM && newDOM.componentDidMount) {
    newDOM.componentDidMount();
  }
}

/**
 * 转换虚拟dom为真实dom
 * @param vdom 虚拟dom
 */
function createDOM(vdom) {
  let { type, props, ref } = vdom;
  let dom; //真实dom

  if (type && type.$$typeof === REACT_FORWARD_REF_TYPE) {
    return mountForwardComponent(vdom);
  } else if (type && type.$$typeof === REACT_PROVIDER) {
    //PROVIDER
    return mountProviderComponent(vdom);
  } else if (type && type.$$typeof === REACT_CONTEXT) {
    //CONSUMER
    return mountContextComponent(vdom);
  } else if (type === REACT_TEXT) {
    dom = document.createTextNode(props);
  } else if (typeof type === "function") {
    return type.isReactComponent //判断是否是类组件
      ? mountClassComponent(vdom)
      : mountFunctionComponent(vdom);
  } else {
    dom = document.createElement(type);
  }
  if (props) {
    //更新dom的属性
    updateProps(dom, null, props);
    const children = props.children;
    if (typeof children === "object" && children.type) {
      children.mountIndex = 0;
      //继节续挂在子点属性
      mount(children, dom);
    } else if (Array.isArray(children)) {
      reconcileChildren(children, dom);
    }
  }
  vdom.dom = dom;
  if (ref) {
    ref.current = dom;
  }
  return dom;
}
//渲染Context
function mountContextComponent(vdom) {
  let { type, props } = vdom;
  let context = type._context;
  let renderVdom = props.children(context._currentValue); //渲染children
  vdom.oldRenderVdom = renderVdom;
  return createDOM(renderVdom);
}

//渲染Provider
function mountProviderComponent(vdom) {
  let { type, props } = vdom;
  let context = type._context;
  context._currentValue = props.value;
  let renderVdom = props.children; //渲染children
  vdom.oldRenderVdom = renderVdom;
  return createDOM(renderVdom);
}

function mountForwardComponent(vdom) {
  //type: forward生成的组件
  let { type, props, ref } = vdom;
  let renderVdom = type.render(props, ref);
  vdom.oldRenderVdom = renderVdom;
  return createDOM(renderVdom);
}

function mountClassComponent(vdom) {
  //type为这个classComponent
  let { type, props, ref } = vdom;
  const classInstance = new type(props);
  //给虚拟dom添加实例属性
  vdom.classInstance = classInstance;
  //类组件添加context
  if (type.contextType) {
    classInstance.context = type.contextType._currentValue;
  }
  if (ref) {
    ref.current = classInstance;
  }
  //生命周期
  if (classInstance.componentWillMount) {
    classInstance.componentWillMount();
  }
  let renderVdom = classInstance.render();
  vdom.oldRenderVdom = classInstance.oldRenderVdom = renderVdom;
  let dom = createDOM(renderVdom);
  //生命周期
  if (classInstance.componentDidMount) {
    dom.componentDidMount = classInstance.componentDidMount.bind(classInstance);
  }
  return dom;
}

function mountFunctionComponent(vdom) {
  let { type, props } = vdom;
  //执行函数，返回要渲染的虚拟dom
  let renderVdom = type(props);
  //保存老的要渲染的vdom
  vdom.oldRenderVdom = renderVdom;
  return createDOM(renderVdom);
}

function reconcileChildren(children, parentDOM) {
  children.forEach((child, index) => {
    if (child) {
      child.mountIndex = index;
      if (Array.isArray(child)) {
        child.forEach((c, idx) => {
          c.mountIndex = idx;
          mount(c, parentDOM);
        });
      } else {
        mount(child, parentDOM);
      }
    }
  });
}

/**
 * 更新真实dom属性
 * @param dom
 * @param oldProps 老属性
 * @param newProps 新属性
 */
function updateProps(dom, oldProps = {}, newProps = {}) {
  for (let key in newProps) {
    if (key === "children") {
      continue;
    } else if (key === "style") {
      let styleObj = newProps[key];
      for (let attr in styleObj) {
        dom.style[attr] = styleObj[attr];
      }
    } else if (/^on[A-Z].*/.test(key)) {
      //绑定事件
      // dom[key.toLowerCase()] = newProps[key];
      addEvent(dom, key.toLowerCase(), newProps[key]);
    } else {
      dom[key] = newProps[key];
    }
  }
  //如果老属性有 新属性没有，则需要删除，新增 更新
  for (let key in oldProps) {
    if (!newProps.hasOwnProperty(key)) {
      dom[key] = null;
    }
  }
}

export function findDOM(vdom) {
  if (!vdom) return null;
  //如果vdom有dom 说明这个vdom是一个原生组件 span
  if (vdom.dom) {
    return vdom.dom;
  }
  // let oldRenderVdom = vdom.oldRenderVdom;
  let renderVdom = vdom.classInstance
    ? vdom.classInstance.oldRenderVdom
    : vdom.oldRenderVdom;
  return findDOM(renderVdom);
}

/**
 * 进行dom diff
 * @param {*} parentDOM 父真实节点
 * @param {*} oldVdom 老的虚拟dom
 * @param {*} newVdom 新的虚拟dom
 */
export function compareTwoVdom(parentDOM, oldVdom, newVdom, nextDOM) {
  //新老都是null 不操作
  if (!oldVdom && !newVdom) {
    return null;
  } else if (oldVdom && !newVdom) {
    //老的有， 新的没有卸载
    unMountVdom(oldVdom);
  } else if (!oldVdom && newVdom) {
    //走挂载
    let newDOM = createDOM(newVdom);
    if (nextDOM) {
      //如果有后续的兄弟节点，则插入到兄弟节点前面
      parentDOM.insertBefore(newDOM, nextDOM);
    } else {
      parentDOM.appendChild(newDOM);
    }

    if (newDOM.componentDidMount) newDOM.componentDidMount();
  } else if (oldVdom && newVdom && oldVdom.type !== newVdom.type) {
    //节点存在，但是类型不同,
    //卸载老的，挂载新的
    unMountVdom(oldVdom);
    let newDOM = createDOM(newVdom);
    if (nextDOM) {
      //如果有后续的兄弟节点，则插入到兄弟节点前面
      parentDOM.insertBefore(newDOM, nextDOM);
    } else {
      parentDOM.appendChild(newDOM);
    }
    if (newDOM.componentDidMount) newDOM.componentDidMount();
  } else {
    //节点存在，但是类型相同,
    //进行深度比较，比较属性，子节点
    updateElement(oldVdom, newVdom);
  }
}
//深度比较两个树
function updateElement(oldVdom, newVdom) {
  //文本节点
  if (oldVdom.type.$$typeof === REACT_CONTEXT) {
    updateContextComponent(oldVdom, newVdom);
  } else if (oldVdom.type.$$typeof === REACT_PROVIDER) {
    updateProviderComponent(oldVdom, newVdom);
  } else if (oldVdom.type === REACT_TEXT) {
    let currentDOM = (newVdom.dom = findDOM(oldVdom));
    if (oldVdom.props !== newVdom.props) {
      currentDOM.textContent = newVdom.props;
    }
  } else if (typeof oldVdom.type === "string") {
    let currentDOM = (newVdom.dom = findDOM(oldVdom));
    updateProps(currentDOM, oldVdom.props, newVdom.props);
    updateChildren(currentDOM, oldVdom.props.children, newVdom.props.children);
  } else if (typeof oldVdom.type === "function") {
    if (oldVdom.type.isReactComponent) {
      updateClassComponent(oldVdom, newVdom);
    } else {
      updateFunctionComponent(oldVdom, newVdom);
    }
  }
}

/**
 * 更新Provider组件
 * @param {*} oldVdom
 * @param {*} newVdom
 */
function updateProviderComponent(oldVdom, newVdom) {
  if (!findDOM(oldVdom)) return;
  let parentDOM = findDOM(oldVdom).parentNode;
  let { type, props } = newVdom;
  let context = type._context;
  context._currentValue = props.value;
  const newRenderVdom = props.children;
  compareTwoVdom(parentDOM, oldVdom.oldRenderVdom, newRenderVdom);
  newVdom.oldRenderVdom = newRenderVdom;
}

/**
 * 更新Provider组件
 * @param {*} oldVdom
 * @param {*} newVdom
 */
function updateContextComponent(oldVdom, newVdom) {
  if (!findDOM(oldVdom)) return;
  let parentDOM = findDOM(oldVdom).parentNode;
  let { type, props } = newVdom;
  let context = type._context;
  const newRenderVdom = props.children(context._currentValue);
  compareTwoVdom(parentDOM, oldVdom.oldRenderVdom, newRenderVdom);
  newVdom.oldRenderVdom = newRenderVdom;
}

/**
 * 函数组件更新
 * @param {*} oldVdom
 * @param {*} newVdom
 */
function updateFunctionComponent(oldVdom, newVdom) {
  if (!findDOM(oldVdom)) return;
  let parentDOM = findDOM(oldVdom).parentNode;
  let { type, props } = newVdom;
  let newRenderVdom = type(props);
  compareTwoVdom(parentDOM, oldVdom.oldRenderVdom, newRenderVdom);
  newVdom.oldRenderVdom = newRenderVdom;
}

/**
 * 类组件更新
 */
function updateClassComponent(oldVdom, newVdom) {
  //复用老的类组件实例
  let classInstance = (newVdom.classInstance = oldVdom.classInstance);
  if (classInstance.componentWillReceiveProps) {
    classInstance.componentWillReceiveProps(newVdom.props);
  }
  classInstance.updater.emitUpdate(newVdom.props);
}

/**
 *
 * @param {*} parentDOM 父DOM
 * @param {*} oldVChildren 老的虚拟DOM数组（可能会是null undefined 对象 数组）
 * @param {*} newVChildren 新的虚拟DOM数组
 */
function updateChildren(parentDOM, oldVChildren, newVChildren) {
  //数据格式化
  oldVChildren = (
    Array.isArray(oldVChildren) ? oldVChildren : [oldVChildren]
  ).filter((item) => item);
  newVChildren = (
    Array.isArray(newVChildren) ? newVChildren : [newVChildren]
  ).filter((item) => item);

  //把老节点存放到一个以key为属性，节点为值的对象里面
  let keyedOldMap = {};
  let lastPlacedIndex = 0;
  oldVChildren.forEach((oldVChild, index) => {
    keyedOldMap[oldVChild.key || index] = oldVChild;
  });
  //存放需要操作的补丁包
  let patch = [];
  //遍历新的children的虚拟树
  newVChildren.forEach((newVChild, index) => {
    let newKey = newVChild.key || index;
    let oldVChild = keyedOldMap[newKey];
    //如果老的存在 则复用原来的
    if (oldVChild) {
      //更新老节点
      updateElement(oldVChild, newVChild);
      if (oldVChild.mountIndex < lastPlacedIndex) {
        patch.push({
          type: MOVE,
          oldVChild,
          newVChild,
          mountIndex: index,
        });
      }
      delete keyedOldMap[newKey];
      lastPlacedIndex = Math.max(lastPlacedIndex, oldVChild.mountIndex);
    } else {
      patch.push({
        type: PLACEMENT,
        newVChild,
        mountIndex: index,
      });
    }
  });
  //获取所有要移动的老节点
  let moveChildren = patch
    .filter((action) => action.type === MOVE)
    .map((action) => action.oldVChild);
  //操作
  //1.删除需要移动和没有复用到的节点
  Object.values(keyedOldMap)
    .concat(moveChildren)
    .forEach((oldVChild) => {
      let currentDOM = findDOM(oldVChild);
      parentDOM.removeChild(currentDOM);
    });
  //打补丁
  patch.forEach((action) => {
    let { type, oldVChild, newVChild, mountIndex } = action;
    let childNodes = parentDOM.childNodes;
    let currentDOM = null;
    //插入
    if (type === PLACEMENT) {
      currentDOM = createDOM(newVChild);
    } else if (type === MOVE) {
      //移动
      currentDOM = findDOM(oldVChild);
    }
    let childNode = childNodes[mountIndex];
    if (childNode) {
      parentDOM.insertBefore(currentDOM, childNode);
    } else {
      parentDOM.appendChild(currentDOM);
    }
  });
}

function unMountVdom(oldVdom) {
  let { props, ref } = oldVdom;
  //获取当前的真实dom
  let currentDom = findDOM(oldVdom);
  //生命周期
  //类组件
  if (oldVdom.classInstance && oldVdom.classInstance.componentWillUnmount) {
    oldVdom.classInstance.componentWillUnmount();
  }

  if (ref) {
    ref.current = null;
  }

  if (props.children) {
    let children = Array.isArray(props.children)
      ? props.children
      : [props.children];
    children.forEach(unMountVdom);
  }

  //把此虚拟dom对应的老的DOM节点从父节点中移除
  if (currentDom) {
    currentDom.parentNode.removeChild(currentDom);
  }
}

const ReactDOM = {
  render,
};

export default ReactDOM;
